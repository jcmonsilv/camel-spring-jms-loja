package br.com.ng5.jcmonsilv.puc.camel.routes;

import lombok.extern.slf4j.Slf4j;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JmsTestRouter extends RouteBuilder {
	
	@Value("${input.queue}")
	private String inboundQueue;
	
	@Value("${output.queue}")
	private String outboundQueue;
	
    @Override
    public void configure() throws Exception {
        System.out.println("Configuring route");

        from(this.inboundQueue)
                .log(LoggingLevel.DEBUG, log, "New message received")
                .process(exchange -> {
                    String convertedMessage = exchange.getMessage().getBody() + " is converted";
                    exchange.getMessage().setBody(convertedMessage);
                })
                .to(this.outboundQueue)
                .log(LoggingLevel.DEBUG, log, "Message sent to the other queue")
        .end();

    }
}