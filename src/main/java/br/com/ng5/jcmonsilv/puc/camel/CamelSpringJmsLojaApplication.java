package br.com.ng5.jcmonsilv.puc.camel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CamelSpringJmsLojaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CamelSpringJmsLojaApplication.class, args);
	}

}
